<?php

/**
 * @file
 * Flle for callback functions of drubot.
 */

/**
 * Display badword form and list of added bad words.
 */
function drubot_badwords() {
  $output = '';
  $form = drupal_get_form('drubot_badwords_form');
  $output .= drupal_render($form);
  $output .= drubot_offensive_words_list();
  return $output;
}
