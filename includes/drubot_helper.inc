<?php

/**
 * @file
 * Flle for helper functions of drubot.
 */

/**
 * Performs a exact match query.
 *
 * @param string $request
 *   A string of request for drubot.
 *
 * @return array
 *   Returns array of response by drubot.
 */
function drubot_exact_match($request) {
  $query = db_select('aiml_tags', 'ati');
  $query->fields('ati', array('template'));
  $query->condition('ati.pattern', $request, '=');
  $result = $query->execute()->fetchAll();
  if (!empty($result[0]->template)) {
    $result[0]->template = drubot_replace_badwords($result[0]->template);
    return ($result[0]->template);
  }
}

/**
 * Performs a approx match query.
 *
 * @param string $request
 *   A string of request for drubot.
 *
 * @return array
 *   Returns array of response by drubot.
 */
function drubot_approx_match($request) {
  $first_word = get_first_word($request);
  $last_word = get_last_word($request);
  $query = db_select('aiml_tags', 'ati');
  $query->fields('ati', array('template'));
  $query->range(0, 1);
  $query->condition('ati.pattern', '%' . $last_word, 'LIKE');
  if ($first_word != $last_word) {
    $query->condition('ati.pattern', $first_word . '%', 'LIKE');
  }
  $result = $query->execute()->fetchField();
  return ($result);
}

/**
 * Performs a approx match by last word query.
 *
 * @param string $request
 *   A string of request for drubot.
 *
 * @return array
 *   Returns array of response by drubot.
 */
function drubot_approx_match_by_last_word($request) {
  $last_word = get_last_word($request);
  $query = db_select('aiml_tags', 'ati');
  $query->fields('ati', array('template'));
  $query->range(0, 1);
  $query->condition('ati.pattern', '%' . $last_word, 'LIKE');
  $result = $query->execute()->fetchField();
  return ($result);
}

/**
 * Performs a math functions match query.
 *
 * @param string $request
 *   A string of request for drubot.
 *
 * @return array
 *   Returns array of response by drubot.
 */
function drubot_math_functions($request) {
  if (preg_match('/(\d+)(?:\s*)([\+\-\*\/])(?:\s*)(\d+)/', $request, $matches) !== FALSE) {

    $operator = $matches[2];
    switch ($operator) {
      case '+':
        $result2 = $matches[1] + $matches[3];
        break;

      case '-':
        $result2 = $matches[1] - $matches[3];
        break;

      case '*':
        $result2 = $matches[1] * $matches[3];
        break;

      case '/':
        $result2 = $matches[1] / $matches[3];
        break;
    }
    if (!empty($result2)) {
      return $result2;
    }
  }
}

/**
 * Performs a google and wiki query.
 *
 * @param string $request
 *   A string of request for drubot.
 *
 * @return array
 *   Returns array of response by drubot.
 */
function drubot_i_dont_know($request) {
  $request = drubot_replace_badwords($request);
  $google_url = 'http://www.google.com/search?q=' . urlencode($request);
  $wiki_url = 'http://en.wikipedia.org/wiki/' . urlencode($request);
  $search_response_google = "I did not learned about it, though  i Googled and found: <a target=\"_blank\" href=\"$google_url\">$request</a> .May be this helps. ";
  $search_response_wiki = "Also wikipedia says this : <a target=\"_blank\" href=\"$wiki_url\">$request</a> ";
  $response_string = $search_response_google . $search_response_wiki;
  return $response_string;
}

/**
 * Get All list of Added Bad Words.
 */
function drubot_offensive_words_list() {
  $headers = array('Word ID', 'Bad Word', 'Replace With');
  $data = array();
  $query = "SELECT * FROM {drubot_badwords}";
  $result = db_query($query)->fetchAll();
  foreach ($result as $key => $record) {
    $data[$key]['word_id'] = $record->word_id;
    $data[$key]['target_bad_word'] = $record->target_bad_word;
    $data[$key]['replace_with'] = $record->replace_with;
  }
  $output = theme('table', array('header' => $headers, 'rows' => $data));
  return $output;
}
