<?php

/**
 * @file
 * Drush integration for drubot.
 */

/**
 * Implements hook_drush_command().
 */
function drubot_drush_command() {
  $items['drubot-download'] = array(
    'callback' => 'drubot_drush_download',
    'description' => dt('Downloads the required aiml files from Github.'),
  );

  return $items;
}

/**
 * Callback to the ime-download Drush command.
 */
function drubot_drush_download() {
  $args = func_get_args();
  $drubot_path = drush_get_context('DRUSH_DRUPAL_ROOT') . '/' . drupal_get_path('module', 'drubot') . '/includes/drubot_alice_aiml';
  $cmd = 'git clone https://github.com/saurabhtripathi/Alice.Drubot.AIML.git ' . $drubot_path;
  if (drush_shell_exec($cmd)) {
    drush_log(dt('AMIL files were downloaded to path: !path.', array('!path' => $drubot_path)), 'success');
  }
  else {
    drush_log(dt('Drush was unable to download aiml files to !path. Check if folder already exists or try Downloading aiml files manually from the site: https://github.com/saurabhtripathi/Alice.Drubot.AIML. Extract the aiml files inside the default drubot files directory or check help page of Drubot', array('!path' => $drubot_path)) . "\n" . dt(' Faliled Attempted command: !cmd.', array('!cmd' => $cmd)), 'error');
  }
}
