/**
 * @file
 */

(function($) {
  Drupal.behaviors.drubot = {
    attach: function(context) {
      $('#drubot-chat-form').ajaxComplete(function(event, xhr, settings) {
        if (event.target.id == 'drubot-chat-form') {
          var drubot_response_html = $('#talk_cloud').html();
          var drubot_response = $('#talk_cloud').text();

          if (drubot_response.length > 0) {
            $('#drubot-chat-form .image_cloud img.anim_char.anim_char_static').hide();
            $('#drubot-chat-form .image_cloud img.anim_char').not('img.anim_char.anim_char_static').show();
                // alert(drubot_response);
            $('#talk_cloud').html("hmm...");
            jQuery({
              count: 0
            }).animate({
              count: drubot_response.length
            }, {
              duration: drubot_response.length * 100,
              start: function() {
              },
              step: function () {
                $('#talk_cloud').html(drubot_response.substring(0, Math.round(this.count)));
              },
              complete: function() {
                $('#talk_cloud').html(drubot_response_html + '.');
                $('#drubot-chat-form .image_cloud img.anim_char').not('img.anim_char.anim_char_static').hide();
                $('#drubot-chat-form .image_cloud img.anim_char.anim_char_static').show();
              }
            });
          }
        }
      });
    }
  }
})(jQuery);
